export default class Disc {
    public id: string;
    public title: string;
    public artist: string;
    public year: string;
};
