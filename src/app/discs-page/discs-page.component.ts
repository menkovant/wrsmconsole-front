import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import Disc from './model/disc';
import 'rxjs/add/operator/toPromise';
import { ModalService } from '../services';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';

const CurrentDiscsForLayout = gql`
  query CurrentDiscsForLayout {
    discs {
          title
          artist
          year
          id
      }
    }
`;

const submitNewDisc = gql`
  mutation CreateDiscMutation ($input: CreateDisc!) {
    createDiscMutation(input: $input) {
        title
        artist
        year
        id
    }
  }
`;

@Component({
  selector: 'app-discs-page',
  templateUrl: './discs-page.component.html',
  styleUrls: ['./discs-page.component.css']
})
export class DiscsPageComponent implements OnInit {
  loadingpb: boolean;
  showmodal: boolean;
  data: any;
  discs: [Disc];
  public id: string;
  public title: string;
  public artist: string;
  public year: string;
  form: FormGroup;
  private formSubmitAttempt: boolean;
  currentFocus: CurrentFocus;



  constructor(private apollo: Apollo, private modalService: ModalService, private formBuilder: FormBuilder) { }


  ngOnInit() {
    const regexPattern = /^(0|[1-9][0-9]*)$/;
    this.loadingpb = true
    this.data = this.apollo.watchQuery<any>({
      query: CurrentDiscsForLayout
    }).valueChanges;
    this.data.subscribe(({ data }) => {
      this.discs = data.discs;
      this.loadingpb = false;
    });
    this.form = this.formBuilder.group({
      idControl: ['', Validators.required],
      titleControl: ['', Validators.required],
      artistControl: ['', Validators.required],
      yearControl: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
  }



  onSubmit() {
    console.log(this.form);
    if (this.form.valid) {
      this.onAddClick();
      this.closeModal("custom-modal-1");
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    this.formSubmitAttempt = true;
    console.log("validateAllFormFields ")
    Object.keys(formGroup.controls).forEach(field => {

      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onFocus(fieldId: string) {
    this.currentFocus = new CurrentFocus(fieldId);
    console.log("onFocus " + fieldId)
  }


  onFocusedOut(fieldId: string) {
    this.currentFocus = null;
    console.log("onFocusedOut " + fieldId)
  }

  isFieldValid(field: string, fieldId: string) {
    var bool = !this.form.get(field).valid && this.form.get(field).touched && this.formSubmitAttempt;
    return bool;
  }

  onLoad(field: string, fieldId: string) {

    if (this.currentFocus != null && this.currentFocus.fieldId == fieldId) {
      var bool = this.isFieldValid(field, fieldId);
if (this.currentFocus.getCurrentValue() != bool) {
  var element = document.getElementById(this.currentFocus.getFieldId());
  element.focus();
}
      // var element = document.getElementById(this.currentFocus.getFieldId());
     
      // var text = (<HTMLInputElement>element).value;
      // console.log("new value " + text + " old value " + this.currentFocus.getCurrentValue())
      // if ((text.length > 0 && this.currentFocus.getCurrentValue().length == 0) || 
      // (text.length == 0 && this.currentFocus.getCurrentValue().length > 0)) {
      //   element.focus();
      // }
      this.currentFocus.setCurrentValue(bool);
    }

  }




  reset() {
    this.form.reset();
    this.currentFocus = null;
  }

  openModal(id: string) {

    this.showmodal = true;
    console.log("open modal " + id)
    this.modalService.open(id);
  }

  closeModal(id: string) {
    console.log("close modal " + id)
    this.modalService.close(id);
    this.reset();
    this.formSubmitAttempt = false;
  }

  onAddClick(): void {
    const newDisc = new Disc();
    newDisc.id = this.id;
    newDisc.title = this.title;
    newDisc.artist = this.artist;
    newDisc.year = this.year;
    // call the mutation in order to create the new disc
    this.apollo.mutate({ mutation: submitNewDisc, variables: { input: newDisc } }).subscribe(({ data }) => {
      console.log('got data', data);
      this.discs = data.createDiscMutation;
    }, (error) => {
      alert(error)
      console.log('there was an error sending the query', error);
    });

    this.id = null;
    this.title = null;
    this.artist = null;
    this.year = null;
  }
}

class CurrentFocus {
  fieldId: string;
  currentValue: boolean;
  constructor(fieldId: string) {
    this.fieldId = fieldId;
  }
  getFieldId() {
    return this.fieldId
  }
  getCurrentValue() {
    return this.currentValue
  }

  setCurrentValue(currentValue: boolean) {
    // console.log("setCurrentValue "+ currentValue)
    this.currentValue = currentValue;
  }


}
